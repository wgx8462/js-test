// Init camera

function camInit(stream) {
    let cameraView = document.getElementById("cameraview");
    cameraView.srcObject = stream;
    cameraView.play();
}

function camInitFailed(error) {
    console.log("get camera permission : ", error)
}

// main init

function mainInit() {
    // Check navigator media device available
    if (!navigator.mediaDevices || !navigator.mediaDevices.getUserMedia) {
        // Navigator mediaDevices not supported
        alert("Media Device not supported")
        return; // 아무것도 없는 리턴은 이동작은 중지하겠다.
    }

    navigator.mediaDevices.getUserMedia({video: true})
        .then(camInit) //성공했으면 camInit 을 실행 시켜라
        .catch(camInitFailed); // 실패했으면 이걸 실행시켜라
}